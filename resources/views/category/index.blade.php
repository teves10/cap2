@extends('layouts.app')
@section('content')
	<div class="container">
		@if(Session::has('success'))
			<div class="alert alert-success">
				<a href="" class="close" data-dismiss="alert">&times;</a>
				{{Session::get('success')}}
			</div>

		@endif
		<div class="col-sm-6">
			@foreach($categories as $category)
			{{$category->name}}

			@endforeach
		</div>
		<div class="col-sm-6 offset-sm-6">
			<div class="">
				<form method="POST">
					@csrf
					<div class="form-group">
						<label for="name control-label">Name</label>
						<input type="text" class="form-control" name="name" id="name" placeholder="Enter your category name">
						@error('name')
								<span class="text-danger">{{$errors->first('name')}} </span>
							@enderror

					</div>
					<input type="submit" name="" value="submit" class="btn btn-success btn-block">
				</form>
			</div>
		</div>
	</div>
@endsection