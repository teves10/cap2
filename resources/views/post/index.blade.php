@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="col-sm-6 offset-sm-3">
			@if(Session::has('success'))
			<div class="alert alert-success">
				<a href="" class="close" data-dismiss="alert">&times;</a>
				{{Session::get('success')}}
			</div>
			@endif
			<form action="" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="panel panel-default">
					<div class="panel-body p-3 bg-light">
						<div class="form-group{{-- {{$errors->has('title') ? 'has-error' : ""}}  --}}">
							<input type="text" name="title" class="form-control" placeholder="Enter your post title">

							@error('title')
								<span class="text-danger">{{$errors->first('title')}} </span>
							@enderror
						</div>
						<div class="form-group">
							<input type="file" class="form-control" name="image">
						</div>
						<div class="form-group">
							<textarea name="body" rows="8" cols="80" class="form-control" placeholder="Enter your post"></textarea>
							@error('body')
								<span class="text-danger">{{$errors->first('body')}} </span>
							@enderror
							
						</div>
							<input type="submit" value="Post" class="btn btn-primary btn-block">
						</div>
					</div>
				</div>	
			</form>

			@foreach($posts as $post)
		
			
			<div class=" my-5">
    <div class="">
        <div class="col-sm-6 offset-sm-3">
	            <div class="card">

                <div class="card-header"><img src="{{$post->user->profile_picture}}"> {{$post->user->username}}
               </div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>
                    	<h4>
                    		{{$post->title}}
                    	</h4>
                    	
                    </div>
                    	
                         {{$post->body}}
                         @if($post->image != null)
                         	<img src="{{asset($post->image)}}" width="100%" height="600">

                         @endif
                    <div class="card-footer">
                    	<a href="#" class="btn btn-link">Like</a>
                    	<a href="" class="btn btn-link">Dislike</a>
                    	<a href="" class="btn btn-link">Comment</a>
                    </div>

               

                </div>
            </div>
        </div>
    </div>
</div>

			@endforeach
		</div>
	</div>

@endsection