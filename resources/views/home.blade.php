    @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        Welcome {{Auth::user()->username}}
                    <img src="{{Auth::user()->profile_picture}}">

                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>

                      <div class="collapse navbar-collapse" id="navbarColor03">
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item active">
                            <a class="nav-link" href="/post">Post <span class="sr-only">(current)</span></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="/comments">Comments</a>
                          </li>
                        
                        </ul>

                      </div>
                    </nav>

                </div>

            </div>
        </div>


@foreach(Auth::user()->posts as $post)
  <div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><img src="{{Auth::user()->profile_picture}}"> {{Auth::user()->username}}</div>

                <div class="card-body">
                   
    
                    <div>
                      <h4>
                        {{$post->title}}
                      </h4>
                    
                      

                      {{$post->body}}
                         @if($post->image != null)
                          <img src="{{asset($post->image)}}" width="100%" height="600">
                          @endif
                        </div>
                      <div class="card-footer">
                      <a href="#" class="btn btn-link">Like</a>
                      <a href="" class="btn btn-link">Dislike</a>
                      <a href="" class="btn btn-link">Comment</a>
                    </div>

                </div>

            </div>
        </div>
 @endforeach
  
    </div>

</div>
@endsection
