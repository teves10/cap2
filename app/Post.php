<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Category;
use User;

class Post extends Model
{
    public function category(){
    	return $this->belongsTo('App\Category');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    
}
