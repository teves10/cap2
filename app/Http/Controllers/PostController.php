<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use Session;
use Image;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all()->sortByDesc('id');
        return view ('post.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {

        $rules = array(
            'title' => 'required|max:255|unique:posts',
            "image" => "nullable|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap",
            'body' => 'nullable|max:255',
            
        );

        $this->validate($req, $rules);
            
    


        $post = new Post;
        $post->title = $req->title;
        $post->body = $req->body;
        $post->category_id = 1;
        $post->user_id = Auth::user()->id;
      
            $image = $req->file('image');
            $filename = time() . "." . $image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $filename);
          
            // Image::make($image)->resize(800, 600)->save($destination);

              $post->image = $destination.$filename;
       

        $post->save();

        Session::flash('success', 'Post was successfully added.');
        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
