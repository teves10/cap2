<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Session;

class CategoryController extends Controller
{
    public function index(){
    	$categories = Category::all()->sortByDesc('id');
    	return view('category.index')->withCategories($categories);
    }

    public function store(Request $req){

    	$this->validate($req, [
    		'name' => 'required|unique:categories|max:255',
    	]);

    	$category = new Category;
    	$category->name = $req->name;
    	$category->save();

    	Session::flash('success', 'Category was added successfully');
    	return redirect('/category');
    }
}
